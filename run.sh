#!/bin/bash
IMAGE="anilornd/nginx-php-fpm"
TAG="latest"

docker run -tid --restart=always \
-e GIT_REPOSITORY=urlreponya \
-p 80:80 \
--name cms-dev \
$IMAGE:$TAG