#!/bin/sh

# clone git repository on deploy
if [ ! -d /home/htdocs ] ; then
  echo "creating htdocs directory"
  mkdir -p /home/htdocs
  cd /home/htdocs
  git clone ${GIT_REPOSITORY} app
  # remove .git folder
  rm -fr ./app/.git
  chown -R nginx:www-data /home/htdocs
fi

# if docker_hooks/nginx.conf does not exist
# ADD ./config/nginx.conf /etc/nginx/
if [ ! -f /home/htdocs/app/docker_hooks/nginx.conf ]; then
  echo "using default nginx.conf"
  cp /home/defaults/nginx.conf /etc/nginx/ 
else
  echo "overriding default nginx.conf"
  cp /home/htdocs/app/docker_hooks/nginx.conf /etc/nginx/
fi

# if ./docker_hooks/php-fpm.conf 
# ADD ./config/php-fpm.conf /etc/php/
if [ ! -f /home/htdocs/app/docker_hooks/php-fpm.conf ]; then
  echo "using default php-fpm.conf"
  cp /home/defaults/php-fpm.conf /etc/php/
else
  echo "overriding default php-fpm.conf"
  cp /home/htdocs/app/docker_hooks/php-fpm.conf /etc/php/
fi

# ADD ./config/php.ini /etc/php/
if [ ! -f /home/htdocs/app/docker_hooks/php.ini  ]; then
  echo "using default php.ini"
  cp /home/defaults/php.ini /etc/php/
else
  echo "overriding default php.ini"
  cp /home/htdocs/app/docker_hooks/php.ini /etc/php/
fi

# install composer php
echo "installing php composer..."
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# start php-fpm
mkdir -p /home/logs/php-fpm

# start nginx
mkdir -p /home/logs/nginx
mkdir -p /tmp/nginx
chown nginx /tmp/nginx

# fix permissions
chmod -R 775 /home/htdocs
chmod -R 777 /home/logs
chown nginx /home/logs

echo "all services started!"

# Start supervisord and services
/usr/bin/supervisord -n -c /etc/supervisord.conf
